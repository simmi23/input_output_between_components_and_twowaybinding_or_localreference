
import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';

@Component ({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html'
})
export class CockpitComponent {
  // output this data to app component using EventEmitter<>()
  // serverCreated is defined in app component in []
  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();
  @Output('bpCreated') bluePrintAdded = new EventEmitter<{serverName: string, serverContent: string}>();
  // changes in the input tag will be reflected using [(ngModel)](Two way Binding)
  // serverName = '';   -----created local reference using # in html file
  serverDescription = '';
  // @ViewChild to access local reference
  // @ts-ignore
  @ViewChild('serverAnotherName') serverAnothername: ElementRef;
  constructor() {}

  onAddServer(serverNameRef: HTMLInputElement) {
   this.serverCreated.emit({
     // no need of serverName as using local reference
     // serverName: this.serverName,
     // prints this -> [object HTMLInputElement]  if
     // serverNameRef is written
     // so write serverNameRef.value
     // .value method is of HTMLInputElement not of string
     serverName: serverNameRef.value,
     serverContent: this.serverDescription
   });
  }
  onAddBlueprint(serverNameRef: HTMLInputElement) {
    this.bluePrintAdded.emit({
      serverName: serverNameRef.value,
      serverContent: this.serverDescription
    });
  }
}
