import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnChanges,
  SimpleChange,
  SimpleChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy, ContentChild, ElementRef
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  // template: '<app-server-element></app-server-element>',
   templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  // none below means that all the css properties in .css file will be available to all
  // components and if specified (encapsulation: ViewEncapsulation.Emulated) will available
  // to all components
  encapsulation: ViewEncapsulation.None
})
export class ServerElementComponent implements OnInit, OnChanges, DoCheck, AfterContentInit,
  AfterContentChecked,
   AfterViewInit,
   AfterViewChecked,
    OnDestroy {
  // property of components are not accessible outside component so if
  // we want to access property(e.g.: elements(below)) outside component use @Input()
  @Input() elements: {type: string, name: string, content: string};
  // for demo of ngOnChanges()
  @Input() name: string;
  // alias to @Input()  will bind in [srvElement] in tag
  // @Input('srvElement') elements: {type: string, name: string, content: string};
  // -------------------
  // @ts-ignore
  // other component value through @ContentChild(local reference in other component)
  @ContentChild('paragraphContent') header: ElementRef;
  // -------Component Life Cycle--------
  // Demo for Life cycle of components
  constructor() {
    console.log('constructor() called');
  }
  // only this hook receives argument
  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges() called');
    console.log(changes);
  }
  ngOnInit() {
    console.log('ngOnInit() called');
  }
  ngDoCheck(): void {
    console.log('ngDoCheck');
  }
  // called after doCheck and is called only once
  ngAfterContentInit(): void {
    console.log('AfterContentInit called');
  }
  ngAfterContentChecked(): void {
    console.log('After Content Checked');
  }
  ngAfterViewInit(): void {
    console.log('After View Init');
    console.log('Heading' + this.header.nativeElement.textContent);
  }
  ngAfterViewChecked(): void {
    console.log('After view Checked');
  }
  ngOnDestroy(): void {
    console.log('onDestroy Called');
  }

  // output:-
  // constructor() called
  // ngOnChanges() called
  // {elements: SimpleChange}
  // elements: SimpleChange
  // currentValue:
  // content: "Just a test"
  // name: "TestServer"
  // type: "server"
  // __proto__: Object
  // firstChange: true
  // previousValue: undefined
  // __proto__: Object__proto__: Object
  // ngOnInit() called
  // ----------------
  // after we click Change server Element Button
  // ngOnChanges() called
  // {name: SimpleChange}
  // name: SimpleChange
  // currentValue: "Changed!"
  // firstChange: false
  // previousValue: "TestServer"
  // ---------------------------------
  // DoCheck gets called after every check(/change) angular notices
  // ---------------
  // after view init we get access to values in html template
}
