import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ServerElementComponent } from './server-element/server-element.component';
import {CockpitComponent} from './cockpit/cockpit.component';
import {FormsModule} from '@angular/forms';
import { TestNgContsntComponent } from './test-ng-contsnt/test-ng-contsnt.component';
import { BetterDirectiveDirective } from './better-directive.directive';

@NgModule({
  declarations: [
    AppComponent,
    ServerElementComponent,
    CockpitComponent,
    TestNgContsntComponent,
    BetterDirectiveDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
